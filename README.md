# Marlin 3D Printer Firmware

I specially modified this to work with the Ender 3 Pro with BLTouch. Use only stock components or nothing that will offset the bltouch position. After installation, you should manually balance the bed once, the do a z-offset. This setup should work fine with Octopi. 

# Marlin PID Autotune Steps

m303 e0 s200 c8; pid auto tune for 200c with 8 iterations


m301 p## i## d##; set new pid

m500; set eprom

m503; verify eprom settings